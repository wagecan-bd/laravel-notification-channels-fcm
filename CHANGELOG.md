## [1.1.1](https://bitbucket.org/wagecan-bd/laravel-notification-channels-fcm/compare/v1.1.0...v1.1.1) (2021-04-06)


### Bug Fixes

* uncatched-exception ([df50939](https://bitbucket.org/wagecan-bd/laravel-notification-channels-fcm/commits/df50939a760517c0748ddbd3423dd0422d185165))

# [1.1.0](https://bitbucket.org/wagecan-bd/laravel-notification-channels-fcm/compare/v1.0.0...v1.1.0) (2021-01-13)


### Bug Fixes

* phpunit version not specified ([7bea595](https://bitbucket.org/wagecan-bd/laravel-notification-channels-fcm/commits/7bea59599a2ccacae0517a3162706e1b76d56627))


### Features

* support for laravel 8 ([1fc1670](https://bitbucket.org/wagecan-bd/laravel-notification-channels-fcm/commits/1fc167072831b3b0fc003ddddcd9b4fe7d8a9f0a))

# [1.0.0](https://bitbucket.org/wagecan-bd/laravel-notification-channels-fcm/compare/v0.0.2...v1.0.0) (2020-01-16)


### Code Refactoring

* for compatiable with laravel 6.x ([69fe764](https://bitbucket.org/wagecan-bd/laravel-notification-channels-fcm/commits/69fe764))


### BREAKING CHANGES

* class NotificationChannels\FCM\FCMMessage rename to Leadbest\NotificationChannels\Fcm\Messages\FCMMessage.
class NotificationChannels\FCM\FCMTarget rename to Leadbest\NotificationChannels\Fcm\Messages\FCMTarget.
