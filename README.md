# Firebase Cloud Messaging notification channel for Laravel 8

## Installation

First please add custom repository config in composer.json

```shell
"repositories":[
    {
        "type": "vcs",
        "url" : "https://bitbucket.org/wagecan-bd/laravel-notification-channels-fcm.git"
    }
]
```

And you can install the package via composer:

```bash
composer require leadbest/laravel-notification-channels-fcm
```

### Setting up your firebase services account file

* Create an gcp service account
* Create an custom iam role with the permission above.
    * cloudmessaging.messages.create
    * firebasenotifications.messages.create
    * firebasenotifications.messages.delete
    * firebasenotifications.messages.get
    * firebasenotifications.messages.list
    * firebasenotifications.messages.update
* Create & download service account key from previous gcp service account
* Finally you can use env `FIREBASE_CREDENTIALS` to specific your service account key file location.

## Usage

In every model you wish to be notifiable via FCM, you can use FCMTarget class to set target for that model accessible through a `routeNotificationForFCM` method:

```php
use Leadbest\NotificationChannels\Fcm\Messages\FCMTarget;

class User extends Authenticatable
{
    use Notifiable;

    public function routeNotificationForFCM($notification)
    {
        return (new FCMTarget('token'))->setTargets($this->device_token);
    }
}
```

You may now tell Laravel to send notifications with FCMMessage class to FCM channels in the `via` method:

```php
// ...
use Illuminate\Notifications\Notification;
use Leadbest\NotificationChannels\Fcm\Messages\FCMMessage;

class UserConnectionCreated extends Notification
{
    public function via($notifiable)
    {
        return ['fcm'];
    }

    public function toFCM($notifiable)
    {
        return (new FCMMessage('This is test push message.')->setTitle('This is test topic.');
    }
}
```

## Some extra features

### retrieve expire tokens during notification send

You can implement method `registerBadTokenCallable` in your Notification class to get expire tokens.

```php
// ...
use Illuminate\Notifications\Notification;

class UserConnectionCreated extends Notification
{
    public function registerBadTokenCallable(array $tokens)
    {
        // do what you need to do.
    }
}
```

### Available Target types

* token : for single device token push.
* topic : for topic push.

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## License

The MIT License (MIT). Please see [LICENSE](LICENSE.md) for more information.
