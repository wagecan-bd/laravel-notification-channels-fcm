<?php

namespace Leadbest\NotificationChannels\Fcm\Tests;

use Illuminate\Notifications\Notification;

class FCMChannelWrongNotification extends Notification
{
    public function toFCM($notifiable)
    {
        return 'not-we-want';
    }
}
