<?php

namespace Leadbest\NotificationChannels\Fcm\Tests;

use Illuminate\Notifications\Notifiable;
use Leadbest\NotificationChannels\Fcm\Messages\FCMTarget;

class FCMChannelTestNotifiable
{
    use Notifiable;

    public function routeNotificationForFCM($notification)
    {
        return (new FCMTarget('token'))->setTargets(['1111-2222-3333-4444']);
    }
}
