<?php

namespace Leadbest\NotificationChannels\Fcm\Tests;

use Illuminate\Notifications\Notifiable;

class FCMChannelWrongNotifiable
{
    use Notifiable;

    public function routeNotificationForFCM($notification)
    {
        return '1111-2222-3333-4444';
    }
}
