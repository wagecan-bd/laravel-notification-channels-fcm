<?php

namespace Leadbest\NotificationChannels\Fcm\Tests;

use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Orchestra\Testbench\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    use MockeryPHPUnitIntegration;

    protected function getPackageProviders($app)
    {
        return [
            \Leadbest\NotificationChannels\Fcm\FCMServiceProvider::class,
            \Kreait\Laravel\Firebase\ServiceProvider::class,
        ];
    }
}
