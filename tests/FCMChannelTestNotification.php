<?php

namespace Leadbest\NotificationChannels\Fcm\Tests;

use Illuminate\Notifications\Notification;
use Leadbest\NotificationChannels\Fcm\Messages\FCMAndroidConfig;
use Leadbest\NotificationChannels\Fcm\Messages\FCMMessage;

class FCMChannelTestNotification extends Notification
{
    public function toFCM($notifiable)
    {
        return (new FCMMessage('push message body'))
        ->setTitle('push message title')
        ->setAndroidConfig(
            (new FCMAndroidConfig())
            ->setChannelId('test channel')
            ->setPriority('high')
        )
        ->setCustomData([
            'customItem1' => 'test content',
            'customItem2' => 'test content 002',
        ]);
    }

    public function registerBadTokenCallable(array $badTokens)
    {
        echo json_encode($badTokens);
    }
}
