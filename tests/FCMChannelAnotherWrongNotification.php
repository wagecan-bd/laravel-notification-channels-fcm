<?php

namespace Leadbest\NotificationChannels\Fcm\Tests;

use Illuminate\Notifications\Notification;

class FCMChannelAnotherWrongNotification extends Notification
{
    public function toNexmo($notifiable)
    {
        return 'not-we-want';
    }
}
