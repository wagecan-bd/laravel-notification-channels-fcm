<?php

namespace Leadbest\NotificationChannels\Fcm\Tests\Src;

use Illuminate\Support\Facades\Notification;
use Leadbest\NotificationChannels\Fcm\Channels\FCMChannel;
use Leadbest\NotificationChannels\Fcm\Tests\TestCase;

class FCMServiceProviderTest extends TestCase
{
    public function testFCMServiceProviderIsWorking()
    {
        $channel = Notification::getFacadeRoot()->driver('fcm');
        $this->assertEquals(get_class($channel), FCMChannel::class);
    }
}
