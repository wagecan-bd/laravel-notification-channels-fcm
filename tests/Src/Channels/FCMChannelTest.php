<?php

namespace Leadbest\NotificationChannels\Fcm\Tests\Src\Channels;

use Mockery as m;
use Kreait\Firebase\Messaging as MessageClient;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Exception\Messaging\AuthenticationError;
use Kreait\Firebase\Exception\Messaging\NotFound;
use Leadbest\NotificationChannels\Fcm\Channels\FCMChannel;
use Leadbest\NotificationChannels\Fcm\Exceptions\InvalidArgument;
use Leadbest\NotificationChannels\Fcm\Exceptions\NotImplementedException;
use Leadbest\NotificationChannels\Fcm\Tests\FCMChannelAnotherWrongNotifiable;
use Leadbest\NotificationChannels\Fcm\Tests\FCMChannelAnotherWrongNotification;
use Leadbest\NotificationChannels\Fcm\Tests\FCMChannelTestNotifiable;
use Leadbest\NotificationChannels\Fcm\Tests\FCMChannelTestNotification;
use Leadbest\NotificationChannels\Fcm\Tests\FCMChannelWrongNotifiable;
use Leadbest\NotificationChannels\Fcm\Tests\FCMChannelWrongNotification;
use Leadbest\NotificationChannels\Fcm\Tests\TestCase;

class FCMChannelTest extends TestCase
{
    protected $notifiable = null;
    protected $notification = null;
    protected $client = null;
    protected $channel = null;

    public function setUp() : void
    {
        parent::setUp();
        $this->notifiable = new FCMChannelTestNotifiable();
        $this->notification = new FCMChannelTestNotification();
        $this->client = m::mock(MessageClient::class);
        $this->channel = new FCMChannel($this->client);
    }

    public function testExceptionNotifiableNotImplementRightMethod()
    {
        $this->expectException(NotImplementedException::class);
        $this->channel->send(new FCMChannelAnotherWrongNotifiable(), $this->notification);
    }

    public function testExceptionNotificationNotImplementRightMethod()
    {
        $this->expectException(NotImplementedException::class);
        $this->channel->send($this->notifiable, new FCMChannelAnotherWrongNotification());
    }

    public function testExceptionNotifiableNotReturnRightObject()
    {
        $this->expectException(InvalidArgument::class);
        $this->channel->send(new FCMChannelWrongNotifiable(), $this->notification);
    }

    public function testExceptionNotificationNotReturnRightObject()
    {
        $this->expectException(InvalidArgument::class);
        $this->channel->send($this->notifiable, new FCMChannelWrongNotification());
    }

    public function testPushIsSentViaFirebase()
    {
        $this->client->shouldReceive('validate')->once();
        $this->client
            ->shouldReceive('send')
            ->withArgs(function (CloudMessage $messageCaller) {
                $callerJsonData = json_encode($messageCaller->jsonSerialize());
                $expectJsonData = json_encode([
                    'data' => [
                        'customItem1' => 'test content',
                        'customItem2' => 'test content 002',
                    ],
                    'notification' => [
                        'title' => 'push message title',
                        'body' => 'push message body',
                    ],
                    'android' => [
                        'priority' => 'high',
                        'notification' => [
                            'channel_id' => 'test channel',
                        ],
                    ],
                    'token' => '1111-2222-3333-4444',
                ]);
                $this->assertJsonStringEqualsJsonString($callerJsonData, $expectJsonData);

                return true;
            })
            ->once();

        $this->channel->send($this->notifiable, $this->notification);
    }

    public function testPushBadTokensViaFirebaseWith400()
    {
        $this->expectOutputString(json_encode(['1111-2222-3333-4444']));
        $this->client
            ->shouldReceive('validate')
            ->once()
            ->andThrow(new InvalidMessage('The registration token is not a valid FCM registration token', 400));

        $this->channel->send($this->notifiable, $this->notification);
    }

    public function testPushBadTokensViaFirebaseWith404()
    {
        $this->expectOutputString(json_encode(['1111-2222-3333-4444']));
        $this->client
            ->shouldReceive('validate')
            ->once()
            ->andThrow(new InvalidMessage('', 404));

        $this->channel->send($this->notifiable, $this->notification);
    }

    public function testPushBadTokensViaFirebaseWith403()
    {
        $this->expectOutputString(json_encode(['1111-2222-3333-4444']));
        $this->client
            ->shouldReceive('validate')
            ->once()
            ->andThrow(new AuthenticationError('', 403));

        $this->channel->send($this->notifiable, $this->notification);
    }

    public function testPushBadTokensViaFirebaseWithNotFound404()
    {
        $this->expectOutputString(json_encode(['1111-2222-3333-4444']));
        $this->client
            ->shouldReceive('validate')
            ->once()
            ->andThrow(new NotFound('', 404));

        $this->channel->send($this->notifiable, $this->notification);
    }
}
