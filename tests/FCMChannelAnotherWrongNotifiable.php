<?php

namespace Leadbest\NotificationChannels\Fcm\Tests;

use Illuminate\Notifications\Notifiable;

class FCMChannelAnotherWrongNotifiable
{
    use Notifiable;

    public function routeNotificationForNexmo($notification)
    {
        return '1111-2222-3333-4444';
    }
}
