<?php

namespace Leadbest\NotificationChannels\Fcm\Messages;

use Leadbest\NotificationChannels\Fcm\Exceptions\InvalidArgument;

class FCMAndroidConfig
{
    const PRIORITY_TYPES = ['normal', 'high'];

    protected $config = [];

    public function setPriority(string $value)
    {
        if (! in_array($value, self::PRIORITY_TYPES)) {
            throw InvalidArgument::invalidInputEnums(
                'FCMAndroidConfig setPriority',
                $value,
                self::PRIORITY_TYPES
            );
        }

        $this->config['priority'] = $value;

        return $this;
    }

    public function setChannelId(string $value)
    {
        $this->config['notification']['channel_id'] = $value;
        return $this;
    }

    public function toArray()
    {
        return count($this->config) > 0 ? $this->config : null;
    }
}
