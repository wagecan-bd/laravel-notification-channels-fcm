<?php

namespace Leadbest\NotificationChannels\Fcm\Messages;

use Illuminate\Support\Arr;

class FCMMessage
{
    protected $title = 'undefined';
    protected $body;
    protected $customData = [];
    protected $androidConfig = null;

    public function __construct(string $body)
    {
        $this->setBody($body);
    }

    public function __get($variable)
    {
        return (isset($this->{$variable})) ? $this->{$variable} : false;
    }

    public function setTitle(string $value)
    {
        $this->title = $value;
        return $this;
    }

    public function setBody(string $value)
    {
        $this->body = $value;
        return $this;
    }

    public function setCustomData(array $value)
    {
        $this->customData = Arr::dot($value);
        return $this;
    }

    public function setAndroidConfig(FCMAndroidConfig $value)
    {
        $this->androidConfig = $value->toArray();
        return $this;
    }
}
