<?php

namespace Leadbest\NotificationChannels\Fcm\Messages;

use Leadbest\NotificationChannels\Fcm\Exceptions\InvalidArgument;
use Illuminate\Support\Arr;

class FCMTarget
{
    const TARGET_TYPES = ['topic', 'token'];

    protected $type;
    protected $values = [];

    public function __construct(string $type)
    {
        $this->setType($type);
    }

    public function __get($variable)
    {
        return (isset($this->{$variable})) ? $this->{$variable} : false;
    }

    public function setType(string $value)
    {
        if (! in_array($value, self::TARGET_TYPES)) {
            throw InvalidArgument::invalidInputEnums(
                'FCMTarget setType',
                $value,
                self::TARGET_TYPES
            );
        }

        $this->type = $value;

        return $this;
    }

    public function setTargets(array $value)
    {
        $this->values = Arr::flatten($value);
        return $this;
    }

    public function addTargets(string $value)
    {
        $this->values[] = $value;
        return $this;
    }
}
