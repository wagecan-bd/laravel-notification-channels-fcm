<?php

namespace Leadbest\NotificationChannels\Fcm\Channels;

use Kreait\Firebase\Messaging as MessageClient;
use Kreait\Firebase\Messaging\Notification as FCMNotification;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Exception\Messaging\AuthenticationError;
use Kreait\Firebase\Exception\MessagingException;
use Illuminate\Notifications\Notification;
use Kreait\Firebase\Exception\Messaging\NotFound;
use Leadbest\NotificationChannels\Fcm\Exceptions\NotImplementedException;
use Leadbest\NotificationChannels\Fcm\Exceptions\InvalidArgument;
use Leadbest\NotificationChannels\Fcm\Messages\FCMMessage;
use Leadbest\NotificationChannels\Fcm\Messages\FCMTarget;

class FCMChannel
{
    const ROUTE_CODE = 'FCM';
    const GET_MESSAGE_CALLABLE = 'toFCM';
    const GET_ROUTE_CALLABLE = 'routeNotificationForFCM';
    const BAD_TOKEN_CALLABLE = 'registerBadTokenCallable';

    protected $fcmClient;

    public function __construct(MessageClient $client)
    {
        $this->fcmClient = $client;
    }

    public function send($notifiable, Notification $notification)
    {
        if (! method_exists($notifiable, self::GET_ROUTE_CALLABLE)) {
            throw NotImplementedException::make(get_class($notifiable), self::GET_ROUTE_CALLABLE);
        }

        if (! method_exists($notification, self::GET_MESSAGE_CALLABLE)) {
            throw NotImplementedException::make(get_class($notification), self::GET_MESSAGE_CALLABLE);
        }

        $targets = $notifiable->routeNotificationFor(self::ROUTE_CODE, $notification);

        if (! $targets instanceof FCMTarget) {
            throw InvalidArgument::invalidInputObjectType(FCMTarget::class, self::GET_ROUTE_CALLABLE);
        }

        $message = $notification->{self::GET_MESSAGE_CALLABLE}($notifiable);

        if (! $message instanceof FCMMessage) {
            throw InvalidArgument::invalidInputObjectType(FCMMessage::class, self::GET_MESSAGE_CALLABLE);
        }

        $badTokens = [];
        foreach ($targets->values as $targetValue) {
            $returnData = $this->__send($targets->type, $targetValue, $message);
            if (! is_null($returnData)) {
                $badTokens[] = $returnData;
            }
        }

        if (! empty($badTokens) && method_exists($notification, self::BAD_TOKEN_CALLABLE)) {
            $notification->{self::BAD_TOKEN_CALLABLE}($badTokens);
        }

        return true;
    }

    protected function __send(string $type, string $target, FCMMessage $message)
    {
        try {
            $notification = FCMNotification::create($message->title, $message->body);

            $messageCaller = CloudMessage::withTarget($type, $target)
                ->withNotification($notification);

            if (count($message->customData)) {
                $messageCaller = $messageCaller->withData($message->customData);
            }

            if (! is_null($message->androidConfig)) {
                $messageCaller = $messageCaller->withAndroidConfig($message->androidConfig);
            }

            $this->fcmClient->validate($messageCaller);
            $this->fcmClient->send($messageCaller);
        } catch (InvalidMessage | AuthenticationError | NotFound $ex) {
            if ($this->detectDeviceTokenInValid($ex)) {
                return $target;
            } else {
                throw $ex;
            }
        }

        return null;
    }

    private function detectDeviceTokenInValid(MessagingException $ex)
    {
        return (
            $ex->getCode() === 400 &&
            $ex->getMessage() === 'The registration token is not a valid FCM registration token'
        ) || (
            $ex->getCode() === 404
        ) || (
            $ex->getCode() === 403
        );
    }
}
