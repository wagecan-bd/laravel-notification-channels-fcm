<?php

namespace Leadbest\NotificationChannels\Fcm;

use Kreait\Firebase\Messaging as MessageClient;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\ChannelManager;
use Leadbest\NotificationChannels\Fcm\Channels\FCMChannel;

class FCMServiceProvider extends ServiceProvider
{
    public function register()
    {
        Notification::resolved(function (ChannelManager $service) {
            $service->extend('fcm', function (Application $app) {
                return $app->make(
                    FCMChannel::class,
                    [
                        $app->make(MessageClient::class),
                    ]
                );
            });
        });
    }
}
