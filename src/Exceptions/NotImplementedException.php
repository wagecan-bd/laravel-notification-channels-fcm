<?php

namespace Leadbest\NotificationChannels\Fcm\Exceptions;

use Exception;

class NotImplementedException extends Exception
{
    public static function make(string $class, string $method)
    {
        return new static(
            sprintf(
                'You should implement method %s in class %s.',
                $method,
                $class
            )
        );
    }
}
