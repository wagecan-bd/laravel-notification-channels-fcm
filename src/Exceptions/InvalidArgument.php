<?php

namespace Leadbest\NotificationChannels\Fcm\Exceptions;

use Exception;

class InvalidArgument extends Exception
{
    public static function configurationNotSet()
    {
        return new static(
            'In order to send notification via Firebase FCM,
            you need to set FIREBASE_CREDENTIALS env to locate service account credential file.'
        );
    }

    public static function invalidInputObjectType(string $className, string $method)
    {
        return new static(
            sprintf(
                'You should use class [%s] when return from method %s in Notification class.',
                $className,
                $method
            )
        );
    }

    public static function invalidInputEnums(string $attr, string $value, array $enumArr)
    {
        return new static(
            sprintf(
                '%s "%s" in not in allow list [%s].',
                $attr,
                $value,
                implode(',', $enumArr)
            )
        );
    }
}
